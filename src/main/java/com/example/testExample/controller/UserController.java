package com.example.testExample.controller;

import com.example.testExample.entity.User;
import com.example.testExample.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Validated
@RestController
@RequestMapping("api/v2")
public class UserController {
    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

//    private final UserRepositoryJpql userRepositoryJpql;


    @GetMapping("/user/{id}")
    public Optional<User> getHouseById(
            @PathVariable(value = "id", required = true) UUID id)  throws DataAccessException {
        try {
            return userRepository.findById(id);
        }
        catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/addUser")
    @Transactional
    public ResponseEntity<User> addDestination(@RequestBody User user) {
        User safeUser = userRepository.save(user);
        return new ResponseEntity<>(safeUser, HttpStatus.OK);
    }

    @DeleteMapping("user/deleteBatchByIdUsers/{id}")
    public void deleteHouse(@PathVariable UUID id) throws DataAccessException {
        List<UUID> listId  = new ArrayList<>();
        listId.add(id);
        userRepository.deleteAllByIdInBatch(listId);
    }

    @PutMapping("/user/putUserName/{uid}")
    @Transactional
    public void putDestination(@RequestBody String name, @PathVariable(value = "uid") UUID uid){
        userRepository.updateName(name,uid);
    }
}


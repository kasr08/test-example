package com.example.testExample.controller;

import com.example.testExample.entity.House;
import com.example.testExample.repository.HouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1")
public class HouseControllerJpa {
    private final HouseRepository houseRepository;

    @GetMapping("/house/{id}")
    public Optional<House> getHouseById(
            @PathVariable(value = "id", required = true) UUID id)  throws DataAccessException {
        try {
            return houseRepository.findById(id);
        }
        catch (RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping("/house/addHouse")
    @Transactional
    public ResponseEntity<House> addDestination(@RequestBody House house) {
        house.setHouseId(UUID.randomUUID());
        House safeHouse = houseRepository.save(house);
        return new ResponseEntity<>(safeHouse, HttpStatus.OK);
    }

    @DeleteMapping("house/deleteBatchByIdHouse/{id}")
    public void deleteHouse(@PathVariable UUID id) throws DataAccessException {
        List<UUID> listId  = new ArrayList<>();
        listId.add(id);
        houseRepository.deleteAllByIdInBatch(listId);

    }

    @PutMapping("/house/putHouseAddress/{houseId}")
    @Transactional
    public void putHomeAddress(@RequestBody String address, @PathVariable(value = "houseId") UUID houseId){
        houseRepository.updateHomeAddress(address,houseId);
    }

    @PutMapping("/house/sellHouse/{userId}/{houseId}")
    @Transactional
    public void sellHouse(@PathVariable(value = "userId") UUID userId, @PathVariable(value = "houseId") UUID houseId){
        houseRepository.sellHouse(userId,houseId);
    }

    @PutMapping("/house/putResidentInHome/{residentId}/{houseID}/{ownerID}")
    @Transactional
    public void putResidentInHome(@PathVariable(value = "residentId") UUID residentId,
                          @PathVariable(value = "houseID") UUID houseID,
                          @PathVariable(value = "ownerID") UUID ownerID){
        houseRepository.putResidentInHome(residentId,houseID,ownerID);
    }

    @PostMapping("/house/addResidentInHome/{residentId}/{houseID}/{ownerID}")
    @Transactional
    public void addResidentInHome(@PathVariable(value = "residentId") UUID residentId,
                                  @PathVariable(value = "houseID") UUID houseID,
                                  @PathVariable(value = "ownerID") UUID ownerID){
        houseRepository.addResidentInHome(residentId,houseID,ownerID);
    }
}

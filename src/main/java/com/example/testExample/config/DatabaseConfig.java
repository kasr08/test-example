package com.example.testExample.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor
public class DatabaseConfig {

    private final ConnectionSettings connectionSettings;

    @Bean
    public DataSource dataSource() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDriverClassName(connectionSettings.getJdbcDriver());
        hikariConfig.setJdbcUrl(connectionSettings.getJdbcString());
        hikariConfig.setUsername(connectionSettings.getJdbcUser());
        hikariConfig.setPassword(connectionSettings.getJdbcPassword());
        hikariConfig.setMinimumIdle(connectionSettings.getJdbcMinPoolIdle());
        hikariConfig.setMaximumPoolSize(connectionSettings.getJdbcMaxPoolSize());
        hikariConfig.setPoolName("main");

        hikariConfig.setConnectionTimeout(10000);
        hikariConfig.setIdleTimeout(10000);
        hikariConfig.setMaxLifetime(120000);
        hikariConfig.addDataSourceProperty("dataSource.networkTimeout", "120000");
        hikariConfig.addDataSourceProperty("dataSource.socketTimeout", "60");
//        hikariConfig.setAutoCommit(false); TODO: https://www.baeldung.com/java-jdbc-auto-commit
        return new HikariDataSource(hikariConfig);
    }

}
package com.example.testExample.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class ConnectionSettings {
    private static int DEFAULT_MIN_POOL_IDLE = 1;
    private static int DEFAULT_MAX_POOL_SIZE = 3;

    @Value("${spring.datasource.driver-class-name}")
    private String jdbcDriver;
    @Value("${spring.datasource.url}")
    private String jdbcString;
    @Value("${spring.datasource.username}")
    private String jdbcUser;
    @Value("${spring.datasource.password}")
    private String jdbcPassword;

    private int jdbcMinPoolIdle = DEFAULT_MIN_POOL_IDLE;
    private int jdbcMaxPoolSize = DEFAULT_MAX_POOL_SIZE;

}

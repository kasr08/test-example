package com.example.testExample.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "houses")
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)

public class House {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UUID id;
    @Column(name = "house_id")
    private UUID houseId;
    @Column(name = "address")
    private String address;
    @Column(name = "owner_id")
    private UUID ownerId;

    @Column(name = "resident_id")
    private UUID residentId;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "u_house_id")
    private List<User> residents;

    public House(UUID id, String address, UUID ownerId, List<User> residents) {
        this.id = id;
        this.address = address;
        this.ownerId = ownerId;
        this.residents = residents;
    }

    public House(UUID id, String address, UUID ownerId) {
        this.id = id;
        this.address = address;
        this.ownerId = ownerId;
    }

    public House(UUID id, String address) {
        this.id = id;
        this.address = address;
    }

    public House() {
    }
}

package com.example.testExample.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com.example.testExample")
@EntityScan("com.example.testExample.entity")
@EnableJpaRepositories("com.example.testExample")
public class TestExampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestExampleApplication.class, args);
    }
}

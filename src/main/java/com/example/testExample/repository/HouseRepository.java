package com.example.testExample.repository;

import com.example.testExample.entity.House;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface HouseRepository extends JpaRepository<House, UUID> {

    @Modifying
    @Query("UPDATE House h SET h.address = :address WHERE h.houseId = :houseId")
    void updateHomeAddress(@Param("address") String address, @Param("houseId") UUID houseId);

    @Modifying
    @Query("UPDATE House h SET h.ownerId = :userId where h.houseId = :houseId")
    void sellHouse(@Param("userId") UUID userId, @Param("houseId") UUID houseId);

    @Modifying
    @Query(value = "UPDATE House h SET h.residentId = :residentId WHERE h.houseId = :houseId and h.ownerId = :ownerId")
    void putResidentInHome( @Param("residentId") UUID residentId, @Param("houseId") UUID houseId, @Param("ownerId") UUID ownerId);

    @Modifying
    @Query(value = "INSERT INTO houses (id, house_id, address, owner_id, resident_id) select gen_random_uuid(),  :houseId, 'ADDRESS', :ownerId, :residentId" +
            " where exists (select * from houses where house_id = :houseId" +
            " and owner_id  = :ownerId) ", nativeQuery = true)
    void addResidentInHome( @Param("residentId") UUID residentId, @Param("houseId") UUID houseId, @Param("ownerId") UUID ownerId);
}


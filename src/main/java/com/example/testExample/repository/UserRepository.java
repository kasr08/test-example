package com.example.testExample.repository;

import com.example.testExample.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public interface UserRepository extends JpaRepository<User, UUID> {

    @Modifying
    @Query(value = "UPDATE User u SET u.name = :name WHERE u.id = :uid")
    void updateName(@Param("name") String name, @Param("uid") UUID uid);
}


CREATE TABLE users (
id uuid NOT NULL DEFAULT uuid_generate_v4(),
name varchar(1000) NOT NULL,
age int NULL,
password varchar(100) NOT NULL,
u_house_id uuid NULL,
PRIMARY KEY (id)
--,
--FOREIGN KEY (house_id) REFERENCES users(id)
)

GO

--INSERT INTO users (id,name,age, password)
--VALUES ("c9dc7589-24fa-4dbd-9d8e-2590b489170f", 'Karen', 24, '2356247'),
--("c9dc7589-24fa-4dbd-9d8e-2590b489170a",'Karegn', 12, '8368736'),
--("c9dc7589-24fa-4dbd-9d8e-2590b489170b",'Karsdgen', 247, '1453456'),
--("c9dc7589-24fa-4dbd-9d8e-2590b489170c",'Ka324ren', 244, '17563'),
--("c9dc7589-24fa-4dbd-9d8e-2590b489170e",'Kafden', 242, '846232'),
--("c9dc7589-24fa-4dbd-9d8e-2590b489170d",'rtyter', 214, '257658');

--GO
--я создаю просто человека, если человек покупает дом, тогда в owner_id должно подставиться id пользователя
CREATE TABLE houses (
id uuid NOT NULL DEFAULT uuid_generate_v4(),
house_id uuid NULL,
address varchar(1000) NOT NULL,
owner_id uuid NULL,
resident_id uuid NULL UNIQUE,
PRIMARY KEY (id))


GO


--INSERT INTO houses (id, address, owner_id)
--VALUES ("c9dc7589-24fa-4dbd-9d8e-2540b489170f",'Маршала Жукова дом 3', "c9dc7589-24fa-4dbd-9d8e-2590b489170a"),
--("c9dc7589-24fa-4dbd-9d8e-2590b489170f",'Маршала Жукова дом 4', "c9dc7589-24fa-4dbd-9d8e-2590b489170b"),
--("c9dc7589-24fa-4dbd-9d8e-2590b489270f",'Маршала Жукова дом 6', "c9dc7589-24fa-4dbd-9d8e-2590b489170c"),
--("c9dc7589-24fa-4dbd-9d8e-2590b489370f",'Маршала Жукова дом 7', "c9dc7589-24fa-4dbd-9d8e-2590b489170d"),
--("c9dc7589-24fa-4dbd-9d8e-2590b489470f",'Маршала Жукова дом 8', "c9dc7589-24fa-4dbd-9d8e-2590b489170f"),


